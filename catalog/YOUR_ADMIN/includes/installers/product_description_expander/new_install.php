<?php
$configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Product Description Expander Configuration' ORDER BY configuration_group_id ASC;");
if ($configuration->RecordCount() > 0) {
  while (!$configuration->EOF) {
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $configuration->MoveNext();
  }
}

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('Product Description Expander Configuration', 'Set Product Description Expander Options', '1', '1');");
$configuration_group_id = $db->Insert_ID();

$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Version', 'PRODUCT_DESCRIPTION_EXPANDER_VERSION', '1.0.0', 'Version installed:', " . $configuration_group_id . ", 0, NOW(), NOW(), NULL, NULL), 
            ('Status', 'PRODUCT_DESCRIPTION_EXPANDER_STATUS', 'false', 'Enable this module?', " . $configuration_group_id . ", 1, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
            ('Enabled Pages', 'PRODUCT_DESCRIPTION_EXPANDER_ENABLED_PAGES', 'product_info, document_general_info, document_product_info', 'Enable this module on these specific pages (comma separated) or enter \'*\' to enable sitewide:', " . $configuration_group_id . ", 10, NOW(), NOW(), NULL, NULL),
            ('Height', 'PRODUCT_DESCRIPTION_EXPANDER_HEIGHT', '60', 'Enter the height in pixels of the product description that should be visible when collapsed:', " . $configuration_group_id . ", 20, NOW(), NOW(), NULL, NULL),
            ('Selector', 'PRODUCT_DESCRIPTION_EXPANDER_SELECTOR', '#productDescription', 'Enter the selector that contains the product description (this is the element that will expand/collapse):', " . $configuration_group_id . ", 20, NOW(), NOW(), NULL, NULL);"
            );