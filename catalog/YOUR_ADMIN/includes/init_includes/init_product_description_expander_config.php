<?php
  if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
  }
  $zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
  // add upgrade script
  if (defined('PRODUCT_DESCRIPTION_EXPANDER_VERSION')) { // does not exist prior to v2.19.0
    $product_description_expander_version = PRODUCT_DESCRIPTION_EXPANDER_VERSION;
    while ($product_description_expander_version != '1.0.2') {
      switch($product_description_expander_version) {
        case '1.0.0':
          // perform upgrade
          if (file_exists(DIR_WS_INCLUDES . 'installers/product_description_expander/1_0_1.php')) {
            include_once(DIR_WS_INCLUDES . 'installers/product_description_expander/1_0_1.php');
            $messageStack->add('Updated Product Description Expander to v1.0.1', 'success');
            $product_description_expander_version = '1.0.1';
			break;
          } else {
          	break 2;
		  }
        case '1.0.1':
          // perform upgrade
          if (file_exists(DIR_WS_INCLUDES . 'installers/product_description_expander/1_0_2.php')) {
            include_once(DIR_WS_INCLUDES . 'installers/product_description_expander/1_0_2.php');
            $messageStack->add('Updated Product Description Expander to v1.0.2', 'success');
            $product_description_expander_version = '1.0.2';
			break;
          } else {
          	break 2;
		  }		                                
        default:
          $product_description_expander_version = '1.0.2';
          // break all the loops
          break 2;      
      }
    }
  } else {
    // begin update to version 2.20.0
    // do a new install
    if (file_exists(DIR_WS_INCLUDES . 'installers/product_description_expander/new_install.php')) {
      include_once(DIR_WS_INCLUDES . 'installers/product_description_expander/new_install.php');
      $messageStack->add('Added Product Description Expander Configuration', 'success');
    } else {
      $messageStack->add('New installation file missing, please make sure you have uploaded all files in the package.', 'error');
    }
  }
  
  if ($zc150) { // continue Zen Cart 1.5.0
    // add configuration menu
    if (!zen_page_key_exists('configProductDescriptionExpander')) {
      $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'PRODUCT_DESCRIPTION_EXPANDER_VERSION' LIMIT 1;");
      $configuration_group_id = $configuration->fields['configuration_group_id'];
      if ((int)$configuration_group_id > 0) {
        zen_register_admin_page('configProductDescriptionExpander',
                                'BOX_CONFIGURATION_PRODUCT_DESCRIPTION_EXPANDER', 
                                'FILENAME_CONFIGURATION',
                                'gID=' . $configuration_group_id, 
                                'configuration', 
                                'Y',
                                $configuration_group_id);
          
        $messageStack->add('Enabled Product Description Expander Configuration menu.', 'success');
      }
    }
  }