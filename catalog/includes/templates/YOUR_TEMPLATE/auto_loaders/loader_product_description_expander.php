<?php
/**
* @package Pages
* @copyright Copyright 2008-2009 RubikIntegration.com
* @copyright Copyright 2003-2006 Zen Cart Development Team
* @copyright Portions Copyright 2003 osCommerce
* @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
* @version $Id: link.php 149 2009-03-04 05:23:35Z yellow1912 $
*/
  if (PRODUCT_DESCRIPTION_EXPANDER_STATUS == 'true') {
    $pages = explode(',', str_replace(' ', '', PRODUCT_DESCRIPTION_EXPANDER_ENABLED_PAGES));
    $loaders[] = array(
      'conditions' => array(
        'pages' => $pages
      ),
		  'jscript_files' => array(
			  'jquery/jquery-1.11.3.min.js' => 1,
        'jquery/jquery_product_description_expander.php' => 2,
			  'jquery/jquery_product_description_expander.js' => 3
		  ),
      'css_files' => array(
          'auto_loaders/product_description_expander.css' => 1,
          'auto_loaders/product_description_expander_overrides.css' => 2
      )
    );
  }
