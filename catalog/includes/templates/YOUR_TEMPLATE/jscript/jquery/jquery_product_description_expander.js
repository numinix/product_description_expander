jQuery(document).ready(function(){
  if ((jQuery(selector).height() - (sliderHeight)) >= 50) {
    jQuery(selector).attr('box_h', jQuery(selector).outerHeight()); 
    jQuery(selector).css({
      'height' : sliderHeight + 'px',
      'text-overflow' : 'ellipsis'
    });
    jQuery(selector).css('overflow', 'hidden');
     jQuery('<div class="slider_menu forward"><div class="rollover" id="readmore"><a href="#">Read More</a></div>').insertAfter(selector);  
    jQuery(".slider_menu a").click(function() { 
      expandDescription();
      return false; 
    });
  }
});

function expandDescription() {
  var open_height = jQuery(selector).attr("box_h") + "px";
  jQuery(selector).animate({"height": open_height}, {duration: "slow" });
  jQuery(".slider_menu").html('<div class="rollover" id="readless"><a href="#">Read Less</a></div>');
  jQuery(".slider_menu a").click(function() { 
    collapseDescription();
    return false; 
  });
}

function collapseDescription() {
  jQuery(selector).animate({"height": sliderHeight}, {duration: "slow" });
  jQuery(".slider_menu").html('<div class="rollover" id="readmore"><a href="#">Read More</a></div>');
  jQuery(".slider_menu a").click(function() { 
    expandDescription();
    return false; 
  });
}